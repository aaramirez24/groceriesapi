from gluon.custom_import import track_changes; track_changes(True)

from datetime import datetime

db.define_table('houseTable', 
    Field('houseId'),
    Field('nickname'),
    Field('manager')  # userId from userTable should go here, don't want to create cyclic reference...
    )

db.define_table ('userTable',
    Field('userId'),
    Field('houseId', 'reference houseTable', default=None),
    Field('firstName'),
    Field('lastName'),
    Field('fullName'),
    Field('accepted', 'boolean', default=False),
    Field('prof_pic', 'upload', uploadfield='prof_blob'),  # The value of 'uploadfield' is the name of the blob field
    Field('prof_blob', 'blob'))

db.define_table('itemTable',
    Field('ownerId'),
    Field('houseId'),
    Field('kitFurn'),
    Field('itemOwner', 'reference userTable'),
    Field('itemName'),
    Field('communal', 'boolean', default=False),
    Field('numRemaining'),
    Field('dateCreated'),
    Field('picture', 'upload', uploadfield='picture_file'),
    Field('picture_file', 'blob')
    )

db.itemTable.dateCreated.default = datetime.utcnow()


db.define_table('allergie',
                Field('user_email', default=auth.user.email if auth.user_id else None),
                Field('allergie_content', 'text'),
                Field('created_on', 'datetime', default=datetime.utcnow()),
                Field('updated_on', 'datetime', update=datetime.utcnow()),
                )


db.define_table('dietrestriction',
                Field('user_email', default=auth.user.email if auth.user_id else None),
                Field('dietrestriction_content', 'text'),
                Field('created_on', 'datetime', default=datetime.utcnow()),
                Field('updated_on', 'datetime', update=datetime.utcnow()),
                )

db.define_table('dislikedfood',
                Field('user_email', default=auth.user.email if auth.user_id else None),
                Field('dislikedfood_content', 'text'),
                Field('created_on', 'datetime', default=datetime.utcnow()),
                Field('updated_on', 'datetime', update=datetime.utcnow()),
                )

