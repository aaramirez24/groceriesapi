// This is the js for the default/index.html view.

var app = function() {

    var self = {};

    Vue.config.silent = false; // show all warnings

    // Extends an array
    self.extend = function(a, b) {
        for (var i = 0; i < b.length; i++) {
            a.push(b[i]);
        }
    };

    function get_allergies_url(start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return allergies_url + "?" + $.param(pp);
    }

    self.get_allergies = function () {
        $.getJSON(get_allergies_url(0, 20), function (data) {
            self.vue.allergies = data.allergies;
            self.vue.has_more_allergies = data.has_more_allergies;
            self.vue.logged_in = data.logged_in;
        })
    };

    self.get_more_allergies = function () {
        var num_allergies = self.vue.allergies.length;
        $.getJSON(get_allergies_url(num_allergies, num_allergies + 50), function (data) {
            self.vue.has_more_allergies  = data.has_more_allergies ;
            self.extend(self.vue.allergies, data.allergies);
        });
    };

    self.add_allergie_button = function () {
        // The button to add a allergie has been pressed.
        self.vue.is_adding_allergie = !self.vue.is_adding_allergie;
    };


    self.add_allergie = function () {
        // The submit button to add a allergie has been added.
        $.post(add_allergie_url,
            {
                allergie: self.vue.text_allergie
            },
            function (data) {
                $.web2py.enableElement($("#add_allergie_submit"));
                self.vue.allergies.unshift(data.allergie); //adds to allergies table,
            });
    };

    self.delete_allergie = function(allergie_id) {
        $.post(del_allergie_url,
            {
                allergie_id: allergie_id
            },
            function () {
                var idx = null;
                for (var i = 0; i < self.vue.allergies.length; i++) {
                    if (self.vue.allergies[i].id === allergie_id) {
                        // If I set this to i, it won't work, as the if below will
                        // return false for items in first position.
                        idx = i + 1;
                        break;
                    }
                }
                if (idx) {
                    self.vue.allergies.splice(idx - 1, 1);
                }
            }
        )
    };

    self.edit_allergie = function () {
    $.post(edit_allergie_url,
        {
            allergie_id: self.vue.allergie_edit_id,
            new_content: self.vue.edited_allergie
        },
        function (data) {
            $.web2py.enableElement($("#edit_allergie_submit"));

            for(var i = 0; i < self.vue.allergies.length;i++){
                if (self.vue.allergies[i].id == data.allergie.id){
                    self.vue.allergies.splice(i,1,data.allergie);


                }
            }
            self.vue.allergie_edit_id = null;

    })};

    self.edit_allergie_button = function (id) {
        // The button to add a allergie has been pressed.
        self.vue.editing_allergie = !self.vue.editing_allergie;
        self.vue.allergie_edit_id = id;
    };

    self.editing_allergier = function (p) {
        return p == self.vue.allergie_edit_id;

    }

   function get_dietrestrictions_url(start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return dietrestrictions_url + "?" + $.param(pp);
    }

    self.get_dietrestrictions = function () {
        $.getJSON(get_dietrestrictions_url(0, 20), function (data) {
            self.vue.dietrestrictions = data.dietrestrictions;
            self.vue.has_more_dietrestrictions  = data.has_more_dietrestrictions;
            self.vue.logged_in = data.logged_in;
        })
    };

    self.get_more_dietrestrictions = function () {
        var num_dietrestrictions = self.vue.dietrestrictions.length;
        $.getJSON(get_dietrestrictions_url(num_dietrestrictions, num_dietrestrictions + 50), function (data) {
            self.vue.has_more_dietrestrictions  = data.has_more_dietrestrictions ;
            self.extend(self.vue.dietrestrictions, data.dietrestrictions);
        });
    };

    self.add_dietrestriction_button = function () {
        // The button to add a dietrestriction has been pressed.
        self.vue.is_adding_dietrestriction = !self.vue.is_adding_dietrestriction;
    };

    self.add_dietrestriction = function () {
        // The submit button to add a dietrestriction has been added.
        $.post(add_dietrestriction_url,
            {
                dietrestriction: self.vue.text_dietrestriction
            },
            function (data) {
                $.web2py.enableElement($("#add_dietrestriction_submit"));
                self.vue.dietrestrictions.unshift(data.dietrestriction); //adds to dietrestrictions table,
            });
    };

    self.delete_dietrestriction = function(dietrestriction_id) {
        $.post(del_dietrestriction_url,
            {
                dietrestriction_id: dietrestriction_id
            },
            function () {
                var idx = null;
                for (var i = 0; i < self.vue.dietrestrictions.length; i++) {
                    if (self.vue.dietrestrictions[i].id === dietrestriction_id) {
                        // If I set this to i, it won't work, as the if below will
                        // return false for items in first position.
                        idx = i + 1;
                        break;
                    }
                }
                if (idx) {
                    self.vue.dietrestrictions.splice(idx - 1, 1);
                }
            }
        )
    };

    self.edit_dietrestriction = function () {
    $.post(edit_dietrestriction_url,
        {
            dietrestriction_id: self.vue.dietrestriction_edit_id,
            new_content: self.vue.edited_dietrestriction
        },
        function (data) {
            $.web2py.enableElement($("#edit_dietrestriction_submit"));

            for(var i = 0; i < self.vue.dietrestrictions.length;i++){
                if (self.vue.dietrestrictions[i].id == data.dietrestriction.id){
                    self.vue.dietrestrictions.splice(i,1,data.dietrestriction);


                }
            }
            self.vue.dietrestriction_edit_id = null;

    })};

    self.edit_dietrestriction_button = function (id) {
        // The button to add a dietrestriction has been pressed.
        self.vue.editing_dietrestriction = !self.vue.editing_dietrestriction;
        self.vue.dietrestriction_edit_id = id;
    };

    self.editing_dietrestrictionr = function (p) {
        return p == self.vue.dietrestriction_edit_id;

    }

    function get_dislikedfoods_url(start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return dislikedfoods_url + "?" + $.param(pp);
    }

    self.get_dislikedfoods = function () {
        $.getJSON(get_dislikedfoods_url(0, 20), function (data) {
            self.vue.dislikedfoods = data.dislikedfoods;
            self.vue.has_more_dislikedfoods  = data.has_more_dislikedfoods ;
            self.vue.logged_in = data.logged_in;
        })
    };

    self.get_more_dislikedfoods = function () {
        var num_dislikedfoods = self.vue.dislikedfoods.length;
        $.getJSON(get_dislikedfoods_url(num_dislikedfoods, num_dislikedfoods + 50), function (data) {
            self.vue.has_more_dislikedfoods  = data.has_more_dislikedfoods ;
            self.extend(self.vue.dislikedfoods, data.dislikedfoods);
        });
    };

    self.add_dislikedfood_button = function () {
        // The button to add a dislikedfood has been pressed.
        self.vue.is_adding_dislikedfood = !self.vue.is_adding_dislikedfood;
    };


    self.add_dislikedfood = function () {
        // The submit button to add a dislikedfood has been added.
        $.post(add_dislikedfood_url,
            {
                dislikedfood: self.vue.text_dislikedfood
            },
            function (data) {
                $.web2py.enableElement($("#add_dislikedfood_submit"));
                self.vue.dislikedfoods.unshift(data.dislikedfood); //adds to dislikedfoods table,
            });
    };

    self.delete_dislikedfood = function(dislikedfood_id) {
        $.post(del_dislikedfood_url,
            {
                dislikedfood_id: dislikedfood_id
            },
            function () {
                var idx = null;
                for (var i = 0; i < self.vue.dislikedfoods.length; i++) {
                    if (self.vue.dislikedfoods[i].id === dislikedfood_id) {
                        // If I set this to i, it won't work, as the if below will
                        // return false for items in first position.
                        idx = i + 1;
                        break;
                    }
                }
                if (idx) {
                    self.vue.dislikedfoods.splice(idx - 1, 1);
                }
            }
        )
    };

    self.edit_dislikedfood = function () {
    $.post(edit_dislikedfood_url,
        {
            dislikedfood_id: self.vue.dislikedfood_edit_id,
            new_content: self.vue.edited_dislikedfood
        },
        function (data) {
            $.web2py.enableElement($("#edit_dislikedfood_submit"));

            for(var i = 0; i < self.vue.dislikedfoods.length;i++){
                if (self.vue.dislikedfoods[i].id == data.dislikedfood.id){
                    self.vue.dislikedfoods.splice(i,1,data.dislikedfood);


                }
            }
            self.vue.dislikedfood_edit_id = null;

    })};

    self.edit_dislikedfood_button = function (id) {
        // The button to add a dislikedfood has been pressed.
        self.vue.editing_dislikedfood = !self.vue.editing_dislikedfood;
        self.vue.dislikedfood_edit_id = id;
    };

    self.editing_dislikedfoodr = function (p) {
        return p == self.vue.dislikedfood_edit_id;

    }


    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
            is_adding_allergie: false,
            allergies: [],
            logged_in: false,
            has_more_allergies: false,
            text_allergie: null,
            edited_allergie: null,
            editing_allergie: false,
            allergie_edit_id: null,



            is_adding_dietrestriction: false,
            dietrestrictions: [],
            has_more_dietrestrictions: false,
            text_dietrestriction: null,
            edited_dietrestriction: null,
            editing_dietrestriction: false,
            dietrestriction_edit_id: null,


            is_adding_dislikedfood: false,
            dislikedfoods: [],
            has_more_dislikedfoods: false,
            text_dislikedfood: null,
            edited_dislikedfood: null,
            editing_dislikedfood: false,
            dislikedfood_edit_id: null

        },
        methods: {
            get_more_allergies: self.get_more_allergies,
            add_allergie_button: self.add_allergie_button,
            add_allergie: self.add_allergie,
            delete_allergie: self.delete_allergie,
            edit_allergie: self.edit_allergie,
            edit_allergie_button: self.edit_allergie_button,
            editing_allergier: self.editing_allergier,




            get_more_dietrestrictions: self.get_more_dietrestrictions,
            add_dietrestriction_button: self.add_dietrestriction_button,
            add_dietrestriction: self.add_dietrestriction,
            delete_dietrestriction: self.delete_dietrestriction,
            edit_dietrestriction: self.edit_dietrestriction,
            edit_dietrestriction_button: self.edit_dietrestriction_button,
            editing_dietrestrictionr: self.editing_dietrestrictionr,


            get_more_dislikedfoods: self.get_more_dislikedfoods,
            add_dislikedfood_button: self.add_dislikedfood_button,
            add_dislikedfood: self.add_dislikedfood,
            delete_dislikedfood: self.delete_dislikedfood,
            edit_dislikedfood: self.edit_dislikedfood,
            edit_dislikedfood_button: self.edit_dislikedfood_button,
            editing_dislikedfoodr: self.editing_dislikedfoodr

        }

    });


    self.get_allergies();
    $("#vue-div").show();
    self.get_dietrestrictions();
    $("#vue-div").show();
    self.get_dislikedfoods();
    $("#vue-div").show();

    return self;
};

var APP = null;

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
jQuery(function(){APP = app();});
// This is the js for the default/index.html view.




