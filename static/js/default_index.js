// This is the js for the default/index.html view.

var app = function() {

    var self = {};

    var spoonacular_key = "5dc85dc4accb47ae9b69830752959225";

    Vue.config.silent = false; // show all warnings

    // Enumerates an array.
    var enumerate = function(v) {
        var k=0;
        return v.map(function(e) {e._idx = k++;});
    };

    self.get_products = function () {
        // Gets new products in response to a query, or to an initial page load.
        $.getJSON(products_url, function(data) {
            self.vue.products = data.products;
            enumerate(self.vue.products);
        });
    };

    self.find_by_ingredients = function () {
        $.getJSON(find_by_ingredients_url, function(data) {
            self.vue.products = data.found_recipes;
            enumerate(self.vue.products);
        });
    }

    self.find_by_ingredients2 = function(){
        ingredient_list = self.vue.product_search
        return axios
            .get("https://api.spoonacular.com/recipes/findByIngredients", {
                params: {
                    "apiKey": spoonacular_key,
                    "ingredients": ingredient_list,
                    "ranking" : 1,
                    "ignorePantry" : true
                }    
            })
            .then(function (response){
                self.vue.recipes = response.data; 
                console.log(response.data)
            })
            .catch(err => console.error(err))

    }

    self.get_item_names = function (){
        var search_term = "";
        for(i = 0; i < self.vue.products.length; i++){
            console.log(i + ": " + self.vue.products[i]);
            search_term = search_term + self.vue.products[i].itemName + ', ';
        };
        self.vue.product_search = search_term;
        console.log(search_term);
        enumerate(self.vue.products);
    };

    self.get_communal_products = function () {
        // Gets new products in response to a query, or to an initial page load.
        $.getJSON(communal_products_url, function(data) {
            self.vue.communal_products = data.communal_items;
            enumerate(self.vue.communal_products);
        });
    };

    self.upload_house_button = function () {
        // The button to add a track has been pressed.
        self.vue.is_house_updating = !self.vue.is_house_updating;
    };

    // add_post function is called to publish a post if there is a user logged in.
    self.add_house = function () {
        // The submit button to add a track has been added.
        $.post(add_house_url,
            {
                //houseId_content: self.vue.form_houseId,
                houseNickname: self.vue.form_nickname
            },
            function (data) {
                $.web2py.enableElement($("#add_home_submit"));
                self.vue.form_houseId = "";
                self.vue.form_nickname = "";
                enumerate(self.vue.houses);
            });
            self.get_houses();
            $("#vue-div").show();
    };

    function get_house_url(start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return houses_url + "?" + $.param(pp);
    };

    self.get_houses = function () {
        $.getJSON(get_house_url(0, 20), function (data) {
            self.vue.houses = data.houses;
            enumerate(self.vue.houses);
        })
    };

    // Complete as needed.
    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
            is_house_updating: false,
            products: [],
            communal_products: [],
            houses:[],
            recipes:[],
            product_search: '',
            form_houseId: null,
            form_nickname: null,
            recipe_json: null
        },
        methods: {
            get_products: self.get_products,
            upload_house_button: self.upload_house_button,
            add_house: self.add_house,
            get_item_names: self.get_item_names,
            find_by_ingredients: self.find_by_ingredients,
            find_by_ingredients2: self.find_by_ingredients2
        }
    });

    self.get_communal_products();
    $("#vue-div").show();

    self.get_products();
    $("#vue-div").show();
   
    self.get_houses();
    $("#vue-div").show();

    self.get_item_names();
    $("#vue-div").show();

    self.find_by_ingredients(); 
    $("vue-div").show();
    
    self.find_by_ingredients2();
    $("vue-div").show(); 


    return self;
};

var APP = null;

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
// So in the Javascript console, type in 
// APP.vue.whatever_variable_you_want_to_look_at
jQuery(function(){APP = app();});

//So in the Javascript console, type in APP.vue.whatever_variable_you_want_to_look_at