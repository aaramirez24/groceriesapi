# Unpack The Pantry 
Living with multiple people can sometimes make it difficult to realize who owns what groceries. This is an attempt to simplify this issue and also ensure that food waste is kept to a minimum. Unpack the Pantry was built to help college students use all food products in their household by keeping track of grocery inventory, and then providing recipes based on that inventory. The current deployment server is hosted [here][Temporary Site]. But will soon be moved to an amazon EC2.

## Getting Started

Follow the instructions in the [web2py repo] to get it installed on your system. If you still have issues, consider referencing the [web2py book].  
Once web2py is up and running, this repository should be cloned in the ```applications/``` folder of the web2py repository. 

### Prerequisites

You will also need API Keys from the following services.
* [Spoonacular] 
* [Firebase Cloud Messaging] One for server, another for android client
* [Gmail] or any other email client that provides SMTP support.

Set the keys and email password as environment variables in your ```~/.bash_profile```. If you are using a linux or mac system it is done as such. 
```
export FCMKEY=your_firebase_server_API_key
export UTPKEY=your_email_password
export SPOON=your_spoonacular_API_key
export DROID=your_android_client_instance_ID
```
Please see the [android-client repository] as well as the [following documentation](https://developers.google.com/instance-id/) for more information on how to get the android instance ID.

### Running the Webserver

To launch the webserver, simply run the `web2py.py` file in the root folder with python. Make sure to run it on your local network. The server should now be available at `localhost:8000/groceriesapi/default/index`. If you would like to send messages from the server to your android device, make sure that you select the public option when prompted. 
![Image of GUI](web2py_GUI.png)
If you are not seeing the above window in linux. Make sure to have the tk library installed.

## Built With

* [Vue.js] - Framework for fast and easy manipulation of DOM elements.
* [Spoonacular] - API used to get recipes. 
* [UniteGallery] - Responsive Image Gallery. 
* [Twitter Bootstrap] - UI boilerplate for modern web apps
* [web2py] - Concise and easy to use python for web apps. 
* [jQuery] - Also used for DOM manipulation. 
* [Firebase Cloud Messaging] - Sends data and notification messages to android clients

## Contributing
Coming Soon.

## Authors

* **[Erick Rodriguez]** - *Initial work and maintainer for both android and server-side clients*
* **[Aaron Ramirez]** - *Initial server work and server Maintainer* - [Temporary Site]
* **[Jose Valencia]** - *Server Contributor*
* **[Esteban Quijada]** - *Server Contributor*
* **[Nishika Tripathi]** - *Initial Logo Design*


## License
Currently under review.

## Acknowledgments

* [Emmanuel Adegbite](https://github.com/olucurious/pyfcm) for the vast simplification of the Firebase Cloud Messaging API
* [Kenneth Reitz](https://github.com/kennethreitz/requests) for the requests library

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [Jose Valencia]: <https://github.com/jolvalen>
   [Aaron Ramirez]: <https://bitbucket.org/aaramirez24>
   [Erick Rodriguez]: <https://bitbucket.org/ergerodr>
   [Nishika Tripathi]: <https://bitbucket.org/ntripat1>
   [Esteban Quijada]: <https://github.com/illconcoctions>

   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [web2py]: <http://web2py.com>
   [jQuery]: <http://jquery.com>
   [Vue.js]: <http://vuejs.org>
   [UniteGallery]: <http://unitegallery.net>
   [Spoonacular]: <https://spoonacular.com/food-api>
   [Firebase Cloud Messaging]: <https://firebase.google.com/docs/cloud-messaging/>
   [Gmail]: <https://support.google.com/a/answer/176600?hl=en>

   [Temporary Site]: <https://960.aaramirez.tk/api/default/index>
   [android-client repository]: <https://bitbucket.org/ergerodr/cmps121_grocery_app>
   [web2py book]: <http://web2py.com/book>
   [web2py repo]: <https://github.com/web2py/web2py>
