from pyfcm import FCMNotification

import os
import logging
import requests
logger = logging.getLogger("web2py.app.fcm")
logger.setLevel(logging.DEBUG)

#Server key used for firebase cloud messaging.
fcm_server_key = os.environ['FCMKEY']

# Specific android client key to send messages to. (My Personal Phone)
client_regid = os.environ['DROID']
push_service = FCMNotification(api_key=fcm_server_key)


def sendTestMessage():
    message_title = "Sending Notification from Web2py Server!"
    message_body = "Test message from UTP"
    result = push_service.notify_single_device(registration_id=client_regid,
                                               message_title=message_title,
                                               message_body=message_body)
    logger.error("Message Sent! Response below: ")
    logger.error(result)
    return result


def requestSync(houseId, userToken):
    data_message = {
        "item_state": "added",
        "houseId": houseId,
        "userToken": userToken
    }
    result = requests.post("https://fcm.googleapis.com/fcm/send", headers={
        "Content-Type": "application/json",
        "Authorization": "key=" + fcm_server_key
    }, json={
        "to": client_regid,
        "data":  data_message })

    logger.error("Item added on Server. Updating mobile devices! Response below: ")
    logger.error(result.json())
    return result
