# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
# This is a sample controller
# - index is the default action of any application
# - user is required for authentication and authorization
# - download is for downloading files uploaded in the db (does streaming)
#########################################################################
import os
import shutil
import json
import requests
import random
import fcm
import json

import kitchen_storage

#python web2py.py -S myapp -M    Interactive mode

""" ENVIRONMENT VARIABLES
put these in your ~/.bash_profile using 'export VAR=your_key_goes_here'
Save environment variable changes using command 'source ~/.bash_profile'
View all environment variables using 'env'
"""
#Key for spoonacular API
spoonacular_key = os.environ['SPOON']
ingredient_list = "Cherry"

#password for gmail account
gmail_key = os.environ['UTPKEY']

from gluon.tools import Mail
from gluon.contrib.user_agent_parser import mobilize

mail = Mail()
mail.settings.server = 'smtp.gmail.com:465'
mail.settings.sender = 'utpmaildaemon@gmail.com'
mail.settings.login = 'utpmaildaemon@gmail.com:' + gmail_key
mail.settings.tls = True
mail.settings.ssl = True

#https://stackoverflow.com/questions/956867/how-to-get-string-objects-instead-of-unicode-from-json
def byteify(input):
    if isinstance(input, dict):
        return {byteify(key): byteify(value)
                for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

#empty test function to design email template
def utp_email_invite():
    return dict()

#sends a test notification to android device. For this to work, the application
#  must be running in the background.
def mobile_test():
    return dict(data=fcm.sendTestMessage())

def index():
    users = db().select(db.userTable.ALL)
    return dict(users=users)

"""  ################## Main Pages Start ###################  """

@auth.requires_login()
def profile():
    user_token = auth.user.user_token if auth.user_id else None
    profile_pic = db(db.userTable.userId == user_token).select(db.userTable.prof_pic).first()
    return dict(profile_pic=profile_pic)

#Edit profile page picture
def edit():
    form = SQLFORM.factory(
        Field('Picture', 'upload', uploadfolder=os.path.join(request.folder, 'uploads'))
    )
    if form.process().accepted:
        user_token = auth.user.user_token if auth.user_id else None
        filename = os.path.join(request.folder, 'uploads/') + form.vars.Picture
        stream = open(filename, 'rb')
        user_record = db(db.userTable.userId == user_token).select().first()
        logger.error(user_record)
        user_record.update_record(prof_pic=db.userTable.prof_pic.store(stream, filename),
                                  prof_blob=stream.read())
        os.remove(filename) #Delete from filesystem, its already in the db.
        redirect('profile')
    elif form.errors:
        session.flash('please select a valid filetype')
    return dict(form=form)

def upload():
    user_token = auth.user.user_token if auth.user_id else None;
    if user_token is None:
        redirect(URL('login'))
    return dict()


def explore():
    results = requests.get("https://api.spoonacular.com/recipes/random",
                           params={
                               "apiKey": spoonacular_key,
                               "number": 2
                           })
    results = byteify(results.json())                       
    logger.error(results)
    logger.error(type(results))
    return dict(results=results)

def create_location():
    form = SQLFORM.factory(
        Field('Name'),
        Field('Furniture_Type'),
        Field('Number_of_Shelves', 'int'),
        Field('Number_of_Doors', 'int'),
        Field('Number_of_Drawers', 'int')
    )
    if form.process().accepted:
        #your logic to create kitchen furniture goes here. 
        #insert into db. 
        #
        redirect(URL('household'))
    kit_obj = kitchen_storage.kitchenStorage("cabinet1", "cabinet", 2, 2, 0)
    return dict(form=form)    


def household():
    userToken = auth.user.user_token if auth.user_id else None
    if userToken is None:
        redirect(URL('login'))
    # select the first record from the usertable that matches the logged-in user's userToken
    record = db(db.userTable.userId == userToken).select().first()
    if record.houseId is None: #If not associated with a house, provide options to join or create one
        logger.error('should return find-create_house')
        auto_complete_search = SQLFORM.factory(Field('Name',
                                                     requires=IS_NOT_EMPTY(),
                                                     widget=SQLFORM.widgets.autocomplete(request,
                                                                                         db.houseTable.nickname,
                                                                                         min_length=1)))
        if auto_complete_search.process().accepted:  # If user joins a house that exists.
            user_record = db(db.userTable.userId == userToken).select().first()
            house_record = db(db.houseTable.nickname == auto_complete_search.vars.Name).select().first()
            if house_record is None:
                session.flash = T("household not found")
                redirect(URL('household'))
            else:
                user_record.update_record(accepted=True, houseId=house_record.id)
                session.flash = T("Joined House")
                redirect(URL('household'))
        elif auto_complete_search.errors:
            response.flash = "Errors in Form"
        return response.render('default/find_create_house.html', dict(
            search_form=auto_complete_search
        ))
    # If person is associated with house.     
    elif record.houseId is not None:
        members = db((db.userTable.houseId == record.houseId) & (db.userTable.userId != record.houseId.manager)).select(db.userTable.prof_pic)
        manager_pic = db(db.userTable.userId == record.houseId.manager).select(db.userTable.prof_pic).first()
        logger.error("members")
        logger.error(members)
        return dict(housename=record.houseId.nickname, manager_pic=manager_pic, manager=True, members=members)
    return dict(housename=record.houseId.nickname)


@auth.requires_login()
def control_panel():
    user_token = auth.user.user_token if auth.user_id else None
    record = db(db.userTable.userId == user_token).select().first()
    find_house = SQLFORM.factory(Field('House_Name',
                                       widget=SQLFORM.widgets.autocomplete(request,
                                                                           db.houseTable.nickname,
                                                                           min_length=1)))
    add_user = SQLFORM.factory(Field('Name',
                                     widget=SQLFORM.widgets.autocomplete(request,
                                                                         db.userTable.fullName,
                                                                         min_length=1)))
    invite_user = SQLFORM.factory(Field('Email',
                                        requires=IS_EMAIL()))
    if invite_user.process(formname='invite_user').accepted:####################################### email form begin
        message = response.render('default/utp_email_invite.html', dict(friend=auth.user.first_name + ' ' + auth.user.last_name))
        status = mail.send(to=[invite_user.vars.Email],
                      subject='Unpack The Pantry Invite!',
                      message='<html>' + message + '</html>')
        if(status):
            response.flash = "Email sent!"
        else:
            response.flash = "Error sending email..."
    elif invite_user.errors:
        response.flash = "Error sending email"############################## email form end
    if find_house.process(formname='find_house').accepted:  ##################################### join house form begin
        user_record = db(db.userTable.userId == user_token).select().first()
        house_record = db(db.houseTable.nickname == find_house.vars.House_Name).select().first()
        item_record = db(db.itemTable.ownerId == user_token).select()

        if house_record is None:
            session.flash = T("household not found, try again")
            redirect(URL('control_panel'))
        else:
            user_record.update_record(accepted=True, houseId=house_record.id)
            """this for loop updates the houseId for each item"""
            for row in item_record:
                row.update_record(houseId=house_record.houseId)
            session.flash = T("Joined House")
            redirect(URL('household'))
    elif find_house.errors:############################################# join house form end
        response.flash = "field cannot be empty"
    if add_user.process(formname='add_user').accepted: ###### add user form
        logger.error("Implement Me!")
    elif add_user.errors:
        response.flash = "field cannot be empty" ##################### add user form end
    return dict(
        invite_user=invite_user,
        house_nickname=record.houseId.nickname,
        find_house=find_house,
        add_user=add_user
    )

@auth.requires_login()
def control_panel_owner():
    return dict()

""" ############## End of Main Pages ############### """

""" ############## Vue/Db Functions Start ########## """
def get_products():
    """Gets the list of products, possibly in response to a query."""
    product_names = ""
    current_user = auth.user.user_token
    product_names = ""
    products = db(db.itemTable.ownerId == current_user).select(db.itemTable.picture,
                                                               db.itemTable.communal,
                                                               db.itemTable.numRemaining,
                                                               db.itemTable.itemName,
                                                               db.itemTable.id)
    for p in products:
        p.image_url = URL('default', 'download', args=p.picture)
        p.edit_url = "location.href='/groceriesapi/default/upload_management/" + str(p.id) + "'"
    #logger.error("get_products")
    #logger.error(products)
    for p in products:
        product_names = product_names + ", "
    return response.json(dict(
        products=products, product_names=product_names
    ))

def findRecipesByIngredients():
    found_recipes = requests.get("https://api.spoonacular.com/recipes/findByIngredients",
                            params={
                                "apiKey": spoonacular_key,
                                "ingredients": ingredient_list,
                                "ranking" : 1,
                                "ignorePantry" : True
                            })    
    found_recipes = byteify(found_recipes.json())
    logger.error(found_recipes)
    return dict(
        results=found_recipes
    )


def get_communal_products():
    """Gets the list of products, possibly in response to a query."""
    userToken = auth.user.user_token if auth.user_id else None
    current_user = db.userTable(db.userTable.userId == userToken)
    results = []
    if current_user:
        #logger.error(current_user.houseId)
        if current_user.houseId:
            products = db(db.itemTable.communal == True).select()
            for p in products:
                (filename, stream) = db.itemTable.picture.retrieve(p.picture)
                shutil.copyfileobj(stream, open(filename, 'wb'))
                p.image_url = URL('default', 'download', args=p.picture)
                results.append(p.image_url)
    return response.json(dict(
        communal_items=results,  # appended to APP.vue.communal_products[]
        result="ok"
    ))

# If the user is not associated with a house, This function will CRASH!
@auth.requires_login()
def upload_management():
    is_edit = (request.vars.edit == 'true')
    if(request.args(0) is not None): form_type = 'edit' if is_edit else 'view'
    if(request.args(0) is None): form_type = 'create'
    user_token = auth.user.user_token
    curr_item = None
    form = SQLFORM.factory(
        Field('Name'),
        Field('Communal', 'boolean'),
        Field('Location'),
        Field('Picture', 'upload', uploadfolder=os.path.join(request.folder, 'uploads')),
        Field('Remaining', 'int')
    )
    q = ((db.userTable.userId == user_token) &
         (db.itemTable.id == request.args(0)))
    curr_item = db(q).select().first()
    #form_type = 'create'
    # Is this an edit form?
    if(is_edit): # Prepopulate form if editing an item.
        form.vars.Name = curr_item.itemTable.itemName
        form.vars.Communal = curr_item.itemTable.communal
        form.vars.Remaining = curr_item.itemTable.numRemaining
        #form.vars.Location = curr_item.itemTable.Location
        form.vars.Picture = URL('default', 'download', host="localhost", port=8000, args=curr_item.itemTable.picture)
        #logger.error(form.vars.Picture)
    button_list = []
    if form_type == 'edit' or form_type == 'create':
        button_list.append(A('Cancel', _class='btn btn-warning',
                                 _href=URL('default', 'upload')))
    elif form_type == 'view':
        button_list.append(A('Edit', _class='btn btn-warning',
                                 _href=URL('default', 'upload_management', args=request.args(0), vars=dict(edit='true'))))
        button_list.append(A('Back', _class='btn btn-primary',
                                 _href=URL('default', 'upload')))
    if form.process().accepted:
        userRecord = db.userTable(db.userTable.userId == user_token)
        #If the form is accepted with no errors
        if(form.vars.Picture is "" and form_type is 'edit'):
            curr_item.itemTable.itemName = form.vars.Name
            curr_item.itemTable.communal = form.vars.Communal
            curr_item.itemTable.numRemaining = form.vars.Remaining
            curr_item.itemTable.Location = form.vars.Location
            #We should have a placeholder picture here. 
            curr_item.itemTable.update_record()
            logger.error("Record Updated")
            redirect(URL('upload'))   
        filename = os.path.join(request.folder, 'uploads/') + form.vars.Picture
        logger.error("Filename is", filename) 
        stream = open(filename, 'rb')
        if(form.vars.Picture is not "" and form_type is 'edit'):
            curr_item.itemTable.itemName = form.vars.Name
            curr_item.itemTable.communal = form.vars.Communal
            curr_item.itemTable.numRemaining = form.vars.Remaining
            curr_item.itemTable.picture = db.itemTable.picture.store(stream, filename)
            curr_item.itemTable.picture_file = stream.read()
            curr_item.itemTable.update_record()
            os.remove(filename)
            logger.error("Record Updated")
            redirect(URL('upload'))
        #logger.error(userRecord)
        if userRecord.houseId is None: #Insert item when user is not associated with house, leave houseId for item blank
            db.itemTable.insert(ownerId=user_token,
                                itemName=form.vars.Name,
                                itemOwner=userRecord,
                                communal=form.vars.Communal,
                                numRemaining=form.vars.Remaining,
                                picture=db.itemTable.picture.store(stream, filename),
                                picture_file=stream.read())
            os.remove(filename) #remove from filesystem, its already in the db.
        else:#insert item when user is associated with house, assign houseId to item
            db.itemTable.insert(ownerId=user_token,
                                houseId=userRecord.houseId.houseId,
                                itemName=form.vars.Name,
                                itemOwner=userRecord,
                                communal=form.vars.Communal,
                                numRemaining=form.vars.Remaining,
                                picture=db.itemTable.picture.store(stream, filename),
                                picture_file=stream.read())
            syncResult = fcm.requestSync(userRecord.houseId.houseId, user_token)
            logger.error(syncResult)
            os.remove(filename) #remove from filesystem, its already in the db.
        redirect(URL('upload'))
    return dict(form=form, form_type=form_type, button_list=button_list, curr_item=curr_item)

def findRecipesByIngredients():
    found_recipes = requests.get("https://api.spoonacular.com/recipes/findByIngredients",
                            params={
                                "ingredients": ingredient_list,
                                "ranking" : 1,
                                "ignorePantry" : true
                            })
    found_recipes = byteify(found_recipes.json())
    logger.error(found_recipes)
    return dict(found_recipes=found_recipes)

# Note that we need the URL to be signed, as this changes the db.
@auth.requires_login()
def add_house():
    user_token = auth.user.user_token if auth.user_id else None
    user_record = db(db.userTable.userId == user_token).select().first()
    """Here you get a new post and add it.  Return what you want."""
    # Implement me!
    db.houseTable.insert(
        houseId=random.random(),
        nickname=request.vars.houseNickname,
        manager=user_token
    )

    house_record = db(db.houseTable.nickname == request.vars.houseNickname).select().first()
    user_record.update_record(accepted=True, houseId=house_record.id) # db change will make this crash

    return dir()


def get_houses():
    # Implement me!
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    # We just generate a lot of of data.
    houses = []
    rows = db().select(db.houseTable.ALL, orderby=~db.houseTable.id, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            # Check if I have a track or not.
            # post_url = URL('api', 'play_track', vars=dict(track_id=r.id)) if r.has_track else None
            t = dict(
                id=r.id,
                houseId=r.houseId,
                nickname=r.nickname
            )
            houses.append(t)
    return response.json(dict(
        houses=houses,
    ))

@auth.requires_login()
def login():
    userToken = auth.user.user_token
    record = db(db.userTable.userId == userToken).select().first()
    if record is None:  # if no entry, create one and mark as not having a house yet. ("active" marks whether or not a
        active = False  # -----------------------------------------------person is with associate with a house.)
        firstName = auth.user.first_name
        lastName = auth.user.last_name
        fullName = firstName + ' ' + lastName
        # db.houseTable.insert(houseId=auth.user_id)
        db.userTable.insert(userId=userToken, firstName=firstName, lastName=lastName, fullName=fullName)
    else:
        active = record.accepted
        firstName = record.firstName
        lastName = record.lastName
    redirect(URL('upload'))
    return dict(name=firstName + " " + lastName, active=active, token=userToken)


def logout():
    auth.logout()
    return dict()


@auth.requires_login()
def groceriesapi():# Go to groceriesapi.py to checkout android response code.
    users = db().select(db.userTable.ALL)
    return dict(users=users)

"""
Necessary for android application to function properly. See user.mobile.html
"""
@mobilize
def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()
