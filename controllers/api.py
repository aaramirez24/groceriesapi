import random


def index():
    pass


# Mocks implementation.
def get_allergies():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    # We just generate a lot of of data.
    allergies = []
    has_more = False
    rows = db().select(db.allergie.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            allergie = dict(
                id=r.id,
                user_email=r.user_email,
                allergie_content=r.allergie_content,
                created_on=r.created_on,
                updated_on=r.updated_on
            )
            allergies.append(allergie)
        else:
            has_more = True
    logged_in = auth.user_id is not None
    return response.json(dict(
        allergies=allergies,
        logged_in=logged_in,
        has_more=has_more,
    ))


@auth.requires_signature()
def add_allergie():
    t_id = db.allergie.insert(
        allergie_content=request.vars.allergie

    )
    t = db.allergie(t_id)
    return response.json(dict(allergie=t))


@auth.requires_signature()
def del_allergie():
    db(db.allergie.id == request.vars.allergie_id).delete()
    return "ok"

@auth.requires_signature()
def edit_allergie():
    p = db.allergie(request.vars.allergie_id)
    p.allergie_content = request.vars.new_content
    p.update_record()
    return response.json(dict(allergie=p))

# Mocks implementation.
def get_dietrestrictions():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    # We just generate a lot of of data.
    dietrestrictions = []
    has_more = False
    rows = db().select(db.dietrestriction.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            dietrestriction = dict(
                id=r.id,
                user_email=r.user_email,
                dietrestriction_content=r.dietrestriction_content,
                created_on=r.created_on,
                updated_on=r.updated_on
            )
            dietrestrictions.append(dietrestriction)
        else:
            has_more = True
    logged_in = auth.user_id is not None
    return response.json(dict(
        dietrestrictions=dietrestrictions,
        logged_in=logged_in,
        has_more=has_more,
    ))


@auth.requires_signature()
def add_dietrestriction():
    t_id = db.dietrestriction.insert(
        dietrestriction_content=request.vars.dietrestriction

    )
    t = db.dietrestriction(t_id)
    return response.json(dict(dietrestriction=t))


@auth.requires_signature()
def del_dietrestriction():
    db(db.dietrestriction.id == request.vars.dietrestriction_id).delete()
    return "ok"

@auth.requires_signature()
def edit_dietrestriction():
    p = db.dietrestriction(request.vars.dietrestriction_id)
    p.dietrestriction_content = request.vars.new_content
    p.update_record()
    return response.json(dict(dietrestriction=p))

# Mocks implementation.
def get_dislikedfoods():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    # We just generate a lot of of data.
    dislikedfoods = []
    has_more = False
    rows = db().select(db.dislikedfood.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            dislikedfood = dict(
                id=r.id,
                user_email=r.user_email,
                dislikedfood_content=r.dislikedfood_content,
                created_on=r.created_on,
                updated_on=r.updated_on
            )
            dislikedfoods.append(dislikedfood)
        else:
            has_more = True
    logged_in = auth.user_id is not None
    return response.json(dict(
        dislikedfoods=dislikedfoods,
        logged_in=logged_in,
        has_more=has_more,
    ))


@auth.requires_signature()
def add_dislikedfood():
    t_id = db.dislikedfood.insert(
        dislikedfood_content=request.vars.dislikedfood

    )
    t = db.dislikedfood(t_id)
    return response.json(dict(dislikedfood=t))


@auth.requires_signature()
def del_dislikedfood():
    db(db.dislikedfood.id == request.vars.dislikedfood_id).delete()
    return "ok"

@auth.requires_signature()
def edit_dislikedfood():
    p = db.dislikedfood(request.vars.dislikedfood_id)
    p.dislikedfood_content = request.vars.new_content
    p.update_record()
    return response.json(dict(dislikedfood=p))
