# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################
import json
import logging
logger = logging.getLogger("web2py.app.api")
logger.setLevel(logging.DEBUG)


def index():
    return dict(message=T('Welcome to web2py!'))



@auth.requires_login()
def login():
    userToken = auth.user.user_token
    record = db(db.userTable.userId==userToken).select().first()
    if record == None: # if no entry, create one and mark as not having a house yet. ("active" marks whether or not a
        active = False # -----------------------------------------------person is with associate with a house.)
        firstName = auth.user.first_name
        lastName = auth.user.last_name
        db.userTable.insert(userId = userToken, firstName = firstName, lastName= lastName)
    else:
        active = record.accepted
        firstName = record.firstName
        lastName = record.lastName
    return dict(name=firstName + " " + lastName, active=active, token=userToken)


########################### ANDROID RESPONSE CODE BELOW ##############################
def get_user(token):
    response.view = 'generic.json'
    #users = db().select(db.userTable.ALL)

    users = db.userTable(1).as_dict()
    return dict(user=users)
    #return response.json(users)

def add_user():
    response.view = 'generic.json'
    # REQURIES houseId, userToken
    #userToken = auth.user.user_token
    #return response.json(userToken)
    userToken = request.vars.userToken
    houseId = request.vars.houseId
    logger.error("add_user: " + userToken + houseId)
    if houseId is None or userToken is None:
        logger.error("Missing arguments")
        result = dict(result="error")
    else :
        logger.error("Appending userList")
        user = db(db.userTable.userId==userToken).select().first()
        house = db(db.houseTable.houseId==houseId).select().first()
        if user is None or house is None:
            logger.error("USER/HOUSE NOT FOUND")
            result = dict(result="error")
        else:
            #house.update_record(userList=house.userList+[user])
            houseMate = []
            user.update_record(accepted=True, houseId=house.id)
            for place in db().select(db.userTable.ALL):
                print ' ', place.houseId
                #print ' ', db.houseTable(place.houseId).houseId, houseId
                if db.houseTable(place.houseId).houseId == houseId:
                    #print ' ', db.houseTable(place.houseId).houseId, houseId
                    houseMate.append({
                        'userId': place.userId,
                        'firstName': place.firstName,
                        'lastName': place.lastName,
                    })
            result = dict(result="ok",houseName=house.nickname,people=houseMate)
    return response.json(result)

def add_house():
    # REQUIRES houseId, nickname
    # RETURNS error: something went wrong
    # RETURNS ok: house added successfuly
    # RETURNS already_exists: name already in use

    logger.error("Creating a new house")
    response.view = 'generic.json'

    nickname = request.vars.nickname
    houseId = request.vars.houseId
    userToken = request.vars.userToken

    logger.error(nickname)
    logger.error(houseId)
    logger.error(userToken)
    if nickname is None or houseId is None or userToken is None:
        logger.error("Missing arguments")
        result = dict(result="error")
    else:
        logger.error("Inserting new house into DB")
        record = db(db.userTable.userId==userToken).select().first()
        if record is None:
            logger.error("USER NOT FOUND")
            result = dict(result="error")
        else:
            if db.houseTable(db.houseTable.nickname==nickname):
                logger.error(db.houseTable.nickname)
                logger.error(nickname + " ALREADY EXISTS")
                result = dict(result="already_exists")
            else:
                #db.userTable[record.id] = dict(houseId=houseId, accepted=True)
                a = db.houseTable.insert(houseId=houseId, nickname=nickname)
		db.userTable[record.id] = dict(houseId=a.id, accepted=True, manager=True)###########
                result = dict(result="ok")
    #logger.error(a)
    #logger.error(db.houseTable[a].userList)
    return response.json(result)

def find_house():
    # REQUIRES nickname, userToken
    # RETURNS error: args missing
    # RETURNS not_found: nickname not found in db
    # RETRUSN ok: house successfuly found
    response.view = 'generic.json'

    nickname = request.vars.nickname
    userToken = request.vars.userToken
    if nickname is None or userToken is None:
        logger.error("Missing arguments")
        result = dict(result="error")
    else:
        logger.error("Retriving house id")
        record = db(db.houseTable.nickname==nickname).select().first()
        if record is None:
            logger.error("House not Found")
            result = dict(result="not_found")
        else: 
            resultList = [dict(houseId=record.houseId)]
            result = dict(result="ok", resultList=resultList)
            # Below is a way to get all the users in the house
            #for users in record.userList:
            #    logger.error(db.userTable[users].firstName)
    return response.json(result)

def get_house_info():
    # REQUIRES userToken
    response.view = 'generic.json'
    userToken = request.vars.userToken
    if userToken is None:
        logger.error("Missing arguments")
        result = dict(result='error')
    else:
        user = db(db.userTable.userId==userToken).select().first()
        if user is None:
            result = dict(result="Error")
        else:
            print user.firstName
            house = db.houseTable[user.houseId]
            houseId = house.houseId
            result= dict(result='ok', houseId=houseId)

    return response.json(result)

def get_items():
    # REQUIRES userToken
    userToken = request.vars.userToken
    houseId = request.vars.houseId
    response.view = 'generic.json'
    #houseRecord = db.houseTable(db.houseTable.houseId==houseId)

    itemList = []
    #if houseRecord is None:
    if userToken is None:
        logger.error("HOUSE NOT FOUND")
        result = dict(result='error')
    else:
        # Dump all items for all users
        for user in db().select(db.userTable.ALL):
	    if user.houseId is None:
		continue
	    else: 
            	if db.houseTable(user.houseId).houseId == houseId:
                	print user.firstName
                	for item in user.itemTable.select():
                    		print ' ', item.itemName
                   		itemList.append({
                            'groceryId' : item.id,
                        	'ownerId': item.ownerId,
                        	'houseId': item.houseId,
                        	'itemOwner': user.userId,
                        	'itemName': item.itemName,
                        	'communal': item.communal,
                        	'numRemaining': item.numRemaining,
                        	'dateCreated': item.dateCreated
                    		})
        result = dict(resultList=itemList,result="ok")
    return response.json(result)


def add_user_item():
    # REQUIRES userToken, itemName, communal, numRemaining
    response.view = 'generic.json'
    userToken = request.vars.userToken
    itemName = request.vars.itemName
    numRemaining = request.vars.numRemaining
    if request.vars.communal is "true":
        communal = True
    else: 
        communal = False
    if userToken is None or itemName is None or numRemaining is None or communal is None:
        logger.error("Missing arguments")
        result = dict(result="error")
    else:
        # Check that user exists
        userRecord = db.userTable(db.userTable.userId==userToken)
        if userRecord is None:
            logger.error("USER DOES NOT EXIST")
            result = dict(result="error")
        else:
            itemRecord = db.itemTable.insert(ownerId=userToken, houseId=userRecord.houseId.houseId, itemName=itemName, communal=communal, numRemaining=numRemaining, itemOwner=userRecord)
            logger.error(itemRecord)
            #itemList = userRecord.itemList
            #if itemList is None:
            #    userRecord.update_record(itemList=itemRecord)
            #else:
            #    userRecord.update_record(itemList=userRecord.itemList+[itemRecord])
            result = dict(result="ok")
    return response.json(result)




def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


